package models

import "time"

type Book struct {
	ID          int       `json:"id" db:"id"`
	Quantity    int       `json:"quantity" db:"quantity"`
	Title       string    `json:"title" db:"title"`
	ISBN        string    `json:"isbn" db:"isbn"`
	ReleaseDate time.Time `json:"release_date" db:"release_date"`
	Genres      []*Genre  `json:"genres"`
}

type AddBookRequest struct {
	Book
	AuthorID int `json:"author_id"`
	GenreID  int `json:"genre_id"`
}
