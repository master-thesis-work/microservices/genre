package database

import (
	"context"
	"genre/pkg/models"
	"github.com/jmoiron/sqlx"
)

type Genrer interface {
	AddGenre(genre *models.Genre) error
	Genre(id int) (*models.Genre, error)
	Genres(page, limit int) ([]*models.Genre, error)
	DeleteGenre(id int) error
}

type Genre struct {
	db *sqlx.DB
}

func NewGenre(db *sqlx.DB) Genrer {
	return &Genre{
		db: db,
	}
}

func (g *Genre) AddGenre(genre *models.Genre) error {
	query := `INSERT INTO genre(name) VALUES ($1)`

	return g.db.QueryRow(query, genre.Name).Err()
}

func (g *Genre) Genre(id int) (*models.Genre, error) {
	genre := new(models.Genre)

	err := g.db.QueryRowx(`SELECT * FROM genre WHERE id = $1`, id).StructScan(genre)
	if err != nil {
		return nil, err
	}

	return genre, nil
}

func (g *Genre) Genres(page, limit int) ([]*models.Genre, error) {
	genres := make([]*models.Genre, 0)

	row, err := g.db.Queryx(`SELECT * FROM genre ORDER BY ID DESC LIMIT $1 OFFSET $2`, limit, page)
	if err != nil {
		return nil, err
	}

	for row.Next() {
		var genre models.Genre

		err = row.StructScan(&genre)
		if err != nil {
			return nil, err
		}

		genres = append(genres, &genre)
	}

	return genres, nil
}

func (g *Genre) DeleteGenre(id int) error {
	tx, err := g.db.BeginTxx(context.Background(), nil)
	if err != nil {
		return err
	}

	tx.MustExec("DELETE FROM book_genre WHERE genre_id = $1", id)
	tx.MustExec("DELETE FROM genre WHERE id = $1", id)

	err = tx.Commit()
	if err != nil {
		e := tx.Rollback()
		if err != nil {
			return e
		}

		return err
	}

	return nil
}
